import { Injectable } from '@nestjs/common';
import { CreateTypeDto } from './dto/create-type.dto';
import { UpdateTypeDto } from './dto/update-type.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Type } from './entities/types.entity';

@Injectable()
export class TypeService {
  constructor(
    @InjectRepository(Type) private typeRepository: Repository<Type>,
  ) {}
  create(createTypeDto: CreateTypeDto) {
    return this.typeRepository.save(createTypeDto);
  }

  findAll() {
    return this.typeRepository.find();
  }

  findOne(id: number) {
    return this.typeRepository.findOneByOrFail({ id });
  }

  async update(id: number, updateTypeDto: UpdateTypeDto) {
    await this.typeRepository.findOneByOrFail({ id });
    await this.typeRepository.update(id, updateTypeDto);
    const updateType = await this.typeRepository.findOneBy({ id });
    return updateType;
  }

  async remove(id: number) {
    const removeType = await this.typeRepository.findOneByOrFail({ id });
    await this.typeRepository.remove(removeType);
    return removeType;
  }
}
