import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RolesModule } from './roles/roles.module';
import { ProductModule } from './product/product.module';
import { UserModule } from './user/user.module';
import { OrdersModule } from './orders/orders.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Order } from './orders/entities/order.entity';
import { Product } from './product/entities/product.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { Type } from './types/entities/types.entity';
import { TypeModule } from './types/type.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [User, Role, Type, Product, Order, OrderItem],
    }),
    RolesModule,
    ProductModule,
    UserModule,
    TypeModule,
    OrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
