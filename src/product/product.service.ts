import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productRepository.save(createProductDto);
  }

  findAll() {
    return this.productRepository.find({});
  }

  findOne(id: number) {
    return this.productRepository.findOneOrFail({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updateProduct = await this.productRepository.findOneOrFail({
      where: { id },
    });
    console.log({ updateProductDto });
    await this.productRepository.update(id, {
      ...updateProduct,
      ...updateProductDto,
    });
    const result = await this.productRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const delProduct = await this.productRepository.findOneOrFail({
      where: { id },
    });
    await this.productRepository.remove(delProduct);
    return delProduct;
  }
}
