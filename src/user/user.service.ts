import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}
  create(createUserDto: CreateUserDto) {
    return this.userRepository.save(createUserDto);
  }

  findAll() {
    return this.userRepository.find({});
  }

  findOne(id: number) {
    return this.userRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.userRepository.findOneOrFail({ where: { id } });
    await this.userRepository.update(id, updateUserDto);
    const updateUser = await this.userRepository.findOneBy({ id });
    return updateUser;
  }

  async remove(id: number) {
    const del = await this.userRepository.findOneOrFail({ where: { id } });
    await this.userRepository.remove(del);
    return del;
  }
}
